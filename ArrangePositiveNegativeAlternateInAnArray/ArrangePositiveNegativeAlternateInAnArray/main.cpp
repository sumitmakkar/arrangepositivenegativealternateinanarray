#include<iostream>
#include<vector>

using namespace std;

class Engine
{
    private:
	   vector<int> arrVector;
    
	   int findRightShiftIndex(int hold)
	   {
           int len = (int)arrVector.size();
           for(int i = hold+1 ; i < len ; i++)
           {
               if((!(hold % 2) && arrVector[i] >= 0) || ((hold % 2) && arrVector[i] < 0))
               {
                   return i;
               }
           }
           return (-1);
       }
    
	   void shiftElementsToRight(int start , int end)
	   {
           int temp = arrVector[end];
           for(int i = end ; i > start ; i--)
           {
               arrVector[i] = arrVector[i-1];
           }
           arrVector[start] = temp;
       }
    
        void displayArrVector()
        {
            int len = (int)arrVector.size();
            for(int i = 0 ; i < len ; i++)
            {
                cout<<arrVector[i]<<" ";
            }
            cout<<endl;
        }
    
    public:
	   Engine(vector<int> aV)
	   {
           arrVector = aV;
       }
    
	   void putPositiveNegativeAlternate()
	   {
           int len = (int)arrVector.size();
           for(int i = 0 ; i < len ; i++)
           {
               int hold = -1;
               if((!(i % 2) && arrVector[i] < 0) || ((i % 2) && arrVector[i] >= 0))
               {
                   hold = i;
               }
               if(hold > -1)
               {
                   int rightShiftIndex = findRightShiftIndex(hold);
                   if(rightShiftIndex > -1)
                   {
                       shiftElementsToRight(hold , rightShiftIndex);
                   }
               }
           }
           displayArrVector();
       }
};

int main()
{
//    int arr[] = {-1,3,2,4,5,-6,7,-9};
    int arr[] = {1,2,-3,5,-6,7,8,9,-5,2,-4,-8,-9,12};
    int len   = sizeof(arr)/sizeof(arr[0]);
    vector<int> arrVector;
    for(int i = 0 ; i < len ; i++)
    {
        arrVector.push_back(arr[i]);
    }
    Engine e = Engine(arrVector);
    e.putPositiveNegativeAlternate();
    return 0;
}
